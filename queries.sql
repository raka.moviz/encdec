update tickets set passenger_name = t2.encrypted_passenger_name from (
   select passenger_name, encode(encrypt(
     decode(passenger_name, 'escape'), '12345678', 'bf-cbc/pad:pkcs'
   ), 'hex') as encrypted_passenger_name from tickets
) t2 where tickets.passenger_name = t2.passenger_name;

select id, passenger_name, convert_from(decrypt(decode(passenger_name, 'hex'), '12345678', 'bf-cbc/pad:pkcs'), 'utf-8') as decrypted_passenger_name from tickets;

drop table if exists ticket_migrations;
drop table if exists tickets;
drop table if exists encryption_keys;

create table encryption_keys ( 
  id int generated always as identity,
  key varchar(8) not null,
  primary key(id)
);

create table tickets (
  id int generated always as identity,
  passenger_name text not null,
  primary key(id)
);

create table ticket_migrations ( 
  id int generated always as identity,
  ticket_id int not null,
  passenger_name_key_id int not null,
  primary key(id),
  constraint fk_ticket foreign key(ticket_id) references tickets(id),
  constraint fk_passenger_name_key foreign key(passenger_name_key_id) references encryption_keys(id)
);

insert into encryption_keys (key) values ('12345678');

insert into tickets (passenger_name) values ('Lara Croft');
insert into tickets (passenger_name) values ('Super Mario');
insert into tickets (passenger_name) values ('Sonic the Hedgehog');

create or replace procedure migrate_tickets()
language plpgsql    
as $$
declare
	mig_tickets_cur refcursor;
	encryption_keys_cur no scroll cursor for select * from encryption_keys order by id desc limit 1;
	mig_ticket record;
	current_encryption_key record;
	passenger_name_decrypted text;
begin
	open encryption_keys_cur;
	fetch encryption_keys_cur into current_encryption_key;
	close encryption_keys_cur;
  
	open mig_tickets_cur no scroll for 
		select t.id t_id, t.passenger_name, tm.id tm_id, tm.passenger_name_key_id, ek.key
		from tickets t 
		left outer join ticket_migrations tm on t.id = tm.ticket_id
		left outer join encryption_keys ek on tm.passenger_name_key_id = ek.id
		where tm.id is null or tm.passenger_name_key_id != current_encryption_key.id;

	loop
		fetch mig_tickets_cur into mig_ticket;
		exit when not found;

		begin
		  if mig_ticket.tm_id is null then
				passenger_name_decrypted := mig_ticket.passenger_name;
			else
				passenger_name_decrypted := convert_from(
					decrypt(decode(mig_ticket.passenger_name, 'hex'), decode(mig_ticket.key, 'escape'), 'bf-cbc/pad:pkcs'), 'utf-8'
				);
			end if;

			update tickets set passenger_name = encode(encrypt(
     		decode(passenger_name_decrypted, 'escape'), decode(current_encryption_key.key, 'escape'), 'bf-cbc/pad:pkcs'
   		), 'hex') where id = mig_ticket.t_id;
			
			if mig_ticket.tm_id is null then
				insert into ticket_migrations (ticket_id, passenger_name_key_id) values (mig_ticket.t_id, current_encryption_key.id);
			else
				update ticket_migrations set passenger_name_key_id = current_encryption_key.id where id = mig_ticket.tm_id;
			end if;
		end;
	end loop;
end;$$


create or replace procedure try_dynamic_sql()
language plpgsql    
as $$
declare
	pii_field text;
  pii_fields text array default array['passenger_name'];

	tickets_cur refcursor; 
	
	-- ticket_rec record; -- tickets;
	ticket_id integer;
	pii_field_value text;
begin
	foreach pii_field in array pii_fields
	loop
		open tickets_cur no scroll for execute format('select id, %I from tickets where id = $1', pii_field) using 1;

		loop
			fetch tickets_cur into ticket_id, pii_field_value;
			exit when not found;

			raise info '%', pii_field_value;
		end loop;
	end loop;
end;$$
