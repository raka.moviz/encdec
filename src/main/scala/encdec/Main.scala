package example

import java.util.Base64
import java.nio.charset.Charset
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec
import javax.crypto.spec.IvParameterSpec


object EncDec extends App {
  def encrypt(password: String, key: String) = {
    val KeyData = key.getBytes(Charset.forName("UTF-8")) 
    if (KeyData.length != 8) {
      throw new IllegalArgumentException("Invalid key size.");
    }

    val KS = new SecretKeySpec(KeyData, "Blowfish") 
    val cipher = Cipher.getInstance("Blowfish/CBC/PKCS5Padding") 
    cipher.init(Cipher.ENCRYPT_MODE, KS, new IvParameterSpec(new Array[Byte](8))) 
    cipher.doFinal(password.getBytes(Charset.forName("UTF-8")))
  }

  def decrypt(encrypted: Array[Byte], key: String) = { 
    val KeyData = key.getBytes(Charset.forName("UTF-8"))
    if (KeyData.length != 8) { 
      throw new IllegalArgumentException("Invalid key size.");
    }
 
    val KS = new SecretKeySpec(KeyData, "Blowfish") 
    val cipher = Cipher.getInstance("Blowfish/CBC/PKCS5Padding") 
    cipher.init(Cipher.DECRYPT_MODE, KS, new IvParameterSpec(new Array[Byte](8))) 
    val decrypted = cipher.doFinal(encrypted) 
    new String(decrypted, Charset.forName("UTF-8")) 
  }

  val message = "message"
  val secretKey = "12345678"

  println(s"message: ${message}")

  val encryptedAsBytes = encrypt(message, secretKey)
  val encryptedAsHexString = encryptedAsBytes.map("%02x" format _).mkString

  println(s"encryptedAsHaxString: ${encryptedAsHexString}")
  
  val decrypted = decrypt(encryptedAsBytes, secretKey)
  println(s"decrypted: ${decrypted}")
}
