import Dependencies._

ThisBuild / scalaVersion     := "2.13.10"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

libraryDependencies ++= Seq(
  "com.chuusai" %% "shapeless" % "2.3.10",
	"io.getquill"	%%	"quill-jdbc-zio" % "4.6.0",
  "org.postgresql"	%	"postgresql"	% "42.5.0"
)

lazy val root = (project in file("."))
  .settings(
    name := "EncDec",
    libraryDependencies += scalaTest % Test
  )

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
